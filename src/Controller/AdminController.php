<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Subscription;
use App\Repository\SubscriptionRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Controller\CardController;
use App\Entity\Card;
use App\Repository\CardRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;

class AdminController extends AbstractFOSRestController
{
    private $userRepository;
    private $subscriptionRepository;
    private $cardRepository;
    private $em;

    public function __construct(CardRepository $cardRepository, SubscriptionRepository $subscriptionRepository,UserRepository $userRepository, EntityManagerInterface $em)
    {
        $this->cardRepository = $cardRepository ;
        $this->subscriptionRepository = $subscriptionRepository ;
        $this->userRepository = $userRepository ;
        $this->em = $em;
    }
    
    /**
     * LIST ALL USERS
     * ============================
     * @Rest\Get("/api/admin/users")
     * @Rest\View(serializerGroups={"user-info"})
     */
    public function getApiUsers(){
        $user = $this->userRepository->findAll();
        return $this->view($user);
    }
    
    /**
     * LIST ONE USER
     * ============================
     * @Rest\Get("/api/admin/user/{email}")
     * @Rest\View(serializerGroups={"user-info"})
     */
    public function getApiUser(User $user){
        return $this->view($user);
    }
    
    /**
     * DELETE ONE USER
     * ============================
     * @Rest\Delete("/api/admin/delete/{email}")
     * @Rest\View(serializerGroups={"user-info3"})
     */
    public function deleteApiUser(User $user, EntityManagerInterface $entityManager){
        $entityManager->remove($user);
        $entityManager->flush();
        return $this->json($user);
    }
    
    
    /**
     * EDIT A USER
     * ============================
     * @Rest\Patch("/api/admin/{email}")
     */
    public function patchApiUser(User $user, Request $request){

        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $adress = $request->get('adress');
        $country = $request->get('country');
        $apiKey = $request->get('apiKey');
        if( null !== $firstname )
        {
            $user->setFirstname($firstname);
        }
        if ( null != $lastname)
        {
            $user->setLastname($lastname);
        }
        if ( null != $adress)
        {
            $user->setAdress($adress);
        }
        if ( null != $country)
        {
            $user->setCountry($country);
        }
        if ( null != $apiKey)
        {
            $user->setapiKey($apiKey);
        }

        $this->em->persist($user);
        $this->em->flush();
        return $this->json($user);
    }
    
    /**
     * CREATE SUBSCRIPTION
     * ============================
     * @Rest\Post("/api/admin/create-sub")
     * @ParamConverter("subscription", converter="fos_rest.request_body")
     */
    public function postApiSubscription(Subscription $subscription, ConstraintViolationListInterface $validationErrors){

        $errors = array();
        if ($validationErrors ->count() > 0) {
            /** @var ConstraintViolation $constraintViolation */
            foreach ($validationErrors as $constraintViolation ){
                // Returns the violation message. (Ex. This value should not be blank.)
                $message = $constraintViolation ->getMessage ();
                // Returns the property path from the root element to the violation. (Ex. lastname)
                $propertyPath = $constraintViolation ->getPropertyPath ();
                $errors[] = ['message' => $message , 'propertyPath' => $propertyPath ];
            }
        }
        if (!empty($errors)) {
            // Throw a 400 Bad Request with all errors messages (Not readable, you can do better)
            throw new BadRequestHttpException(\json_encode( $errors));
        }
        $this->em->persist($subscription);
        $this->em->flush();
        return $this->view($subscription);
    }
    
    
    /**
     * GZT ALL SUBSCRIPTIONS
     * ============================
     * @Rest\Get("/api/admin/subs")
     * @Rest\View(serializerGroups={"sub-info"})
     */
    public function getApiSubscriptions(){
        $subscription = $this->subscriptionRepository->findAll();
        return $this->view($subscription);
    }
    
    /**
     * GET ONE SUBSCRIPTION
     * ============================
     * @Rest\Get("/api/admin/sub/{id}")
     * @Rest\View(serializerGroups={"sub-info"})
     */
    public function getApiSubscription(Subscription $subscription){
        return $this->view($subscription);
    }
    
    /**
     * DELETE ONE SUBSCRIPTION
     * ============================
     * @Rest\Delete("/api/admin/{id}/delete")
     */
    public function deleteApiSubscription(Subscription $subscription, EntityManagerInterface $entityManager){
        $entityManager->remove($subscription);
        $entityManager->flush();
        return $this->json($subscription);
    }

    /**
     * EDIT SUBSCRIPTION
     * ============================
     * @Rest\Patch("/api/admin/{id}/patch")
     */
    public function patchApiSubscription(Subscription $subscription, Request $request,  ValidatorInterface $validator){

        $name = $request->get('name');
        $slogan = $request->get('slogan');
        $url = $request->get('url');
        
        if( null !== $name )
        {
            $subscription->setName($name);
        }
        if ( null != $slogan)
        {
            $subscription->setSlogan($slogan);
        }
        if ( null != $url)
        {
            $subscription->setUrl($url);
        }

        $validationErrors = $validator->validate($subscription);
        $errors = array();
        if ($validationErrors->count() > 0) {

            /** @var ConstraintViolation $constraintViolation */
            foreach ($validationErrors as $constraintViolation ){

                $message = $constraintViolation ->getMessage ();

                $propertyPath = $constraintViolation ->getPropertyPath ();
                $errors[] = ['message' => $message , 'propertyPath' => $propertyPath ];
            }
        }
        if (!empty($errors)) {

            throw new BadRequestHttpException(\json_encode( $errors));

        }

        $this->em->persist($subscription);
        $this->em->flush();
        return $this->json($subscription);
    }
    
    /**
     * CREATE A CARD
     * ============================
     * @Rest\Post("/api/admin/create-card")
     * @ParamConverter("card", converter="fos_rest.request_body")
     */
    public function postApiCard(Card $card, ConstraintViolationListInterface $validationErrors){

        $errors = array();

        if ($validationErrors ->count() > 0) {
            /** @var ConstraintViolation $constraintViolation */
            foreach ($validationErrors as $constraintViolation ){
                // Returns the violation message. (Ex. This value should not be blank.)
                $message = $constraintViolation ->getMessage ();
                // Returns the property path from the root element to the violation. (Ex. lastname)
                $propertyPath = $constraintViolation ->getPropertyPath ();
                $errors[] = ['message' => $message , 'propertyPath' => $propertyPath ];
            }
        }
        if (!empty($errors)) {
            // Throw a 400 Bad Request with all errors messages (Not readable, you can do better)
            throw new BadRequestHttpException(\json_encode( $errors));
        }
        $this->em->persist($card);
        $this->em->flush();
        return $this->view($card);
    }
    
    /**
     * GET ALL CARDS
     * ============================
     * @Rest\Get("/api/admin/cards")
     * @Rest\View(serializerGroups={"card-info"})
     */
    public function getApiCards(){
        $card = $this->cardRepository->findAll();
        return $this->view($card);
    }
    
    /**
     * GET ONLY ONE CARD
     * ============================
     * @Rest\Get("/api/admin/card/{id}")
     * @Rest\View(serializerGroups={"card-info"})
     */
    public function getApiCard(Card $card){
        return $this->view($card);
    }
    
    /**
     * DELETE A CARD
     * ============================
     * @Rest\Delete("/api/admin/card/delete/{id}/delete")
     */
    public function deleteApiCard(Card $card, EntityManagerInterface $entityManager){
        $entityManager->remove($card);
        $entityManager->flush();
        return $this->json($card);
    }

    /**
     * EDIT A CARD
     * ============================
     * @Rest\Patch("/api/admin/{id}/patch")
     * @Rest\View(serializerGroups={"card-info"})
     */
    public function patchApiCard(Card $card, Request $request, ValidatorInterface $validator){

        $name = $request->get('name');
        $slogan = $request->get('slogan');
        $url = $request->get('url');
        if( null !== $name )
        {
            $name->setFirstname($name);
        }
        if ( null != $slogan)
        {
            $slogan->setLastname($slogan);
        }
        if ( null != $url)
        {
            $url->setAdress($url);
        }
        $validationErrors = $validator->validate($card);
        $errors = array();
        if ($validationErrors->count() > 0) {
            /** @var ConstraintViolation $constraintViolation */
            foreach ($validationErrors as $constraintViolation ){
                $message = $constraintViolation ->getMessage ();

                $propertyPath = $constraintViolation ->getPropertyPath ();
                $errors[] = ['message' => $message , 'propertyPath' => $propertyPath ];
            }
        }
        if (!empty($errors)) {
            throw new BadRequestHttpException(\json_encode( $errors));
        }

        $this->em->persist($card);
        $this->em->flush();
        return $this->json($card);
    }
    
}
