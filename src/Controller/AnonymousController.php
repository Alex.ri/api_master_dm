<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\Patch;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\EntityManager;
use App\Entity\Article;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Repository\UserRepository;


class AnonymousController extends AbstractFOSRestController
{
    private $userRepository;
    private $em;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $em)
    {
        $this->userRepository = $userRepository ;
        $this->em = $em;
    }

    /**
     * GET ALL USERS INFORMATION
     * ============================
     * @Rest\Get("/api/anonymous")
     * @Rest\View(serializerGroups={"anonymes"})
     */
    public function getApiUsers(){
        $user = $this->userRepository->findAll();
        return $this->view($user);
    }

    /**
     * GET ONE USER
     * ============================
     * @Rest\Get("/api/anonymous/{id}")
     * @Rest\View(serializerGroups={"anonymes"})
     */
    public function getApiUser(User $user){
        return $this->view($user);
    }

    /**
     * GET ALL SUBSCRIPTIONS
     * ============================
     * @Rest\Get("/api/anonymous_subs")
     * @Rest\View(serializerGroups={"subs"})
     */
    public function getApiSubs(){
        $user = $this->userRepository->findAll();
        return $this->view($user);
    }

    /**
     * GET ONE SUBSCRIPTION
     * ============================
     * @Rest\Get("/api/anonymous_sub/{id}")
     * @Rest\View(serializerGroups={"sub"})
     */
    public function getApiSub(User $user){
        return $this->view($user);
    }

    /**
     * CREATE A USER
     * ============================
     * @Rest\Post("/api/anonymous")
     * @ParamConverter("user", converter="fos_rest.request_body")
     * @Rest\View(serializerGroups={"anonyme"})
     */
    public function postApiUser(User $user, ConstraintViolationListInterface $validationErrors){

        $errors = array();
        if ($validationErrors ->count() > 0) {
            /** @var ConstraintViolation $constraintViolation */
            foreach ($validationErrors as $constraintViolation ){
                // Returns the violation message. (Ex. This value should not be blank.)
                $message = $constraintViolation ->getMessage ();
                // Returns the property path from the root element to the violation. (Ex. lastname)
                $propertyPath = $constraintViolation ->getPropertyPath ();
                $errors[] = ['message' => $message , 'propertyPath' => $propertyPath ];
            }
        }
        if (!empty($errors)) {
            // Throw a 400 Bad Request with all errors messages (Not readable, you can do better)
            throw new BadRequestHttpException(\json_encode( $errors));
        }
        $this->em->persist($user);
        $this->em->flush();
        return $this->view($user);
    }
}
