<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\Patch;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\EntityManager;
use App\Entity\Article;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Card;
use App\Repository\CardRepository;
use Symfony\Component\Config\Definition\Exception\Exception;

class UserController extends AbstractFOSRestController
{
    private $cardRepository;
    private $userRepository;
    private $em;

    public function __construct(CardRepository $cardRepository, UserRepository $userRepository, EntityManagerInterface $em)
    {
        $this->cardRepository = $cardRepository ;
        $this->userRepository = $userRepository ;
        $this->em = $em;
    }

    /**
     * GET ALL USER INFORMATIONS
     * =========================
     * @Rest\Get("/api/user")
     * @Rest\View(serializerGroups={"user-info"})
     */
    public function getApiUsers(){
        $user = $this->userRepository->findAll();
        return $this->view($user);
    }

    /**
     * GET MY USER INFORMATION
     * ============================
     * @Rest\Get("/api/user/{email}")
     * @Rest\View(serializerGroups={"user-info"})
     */
    public function getApiUser(User $user){
        $apiEmail=$user->getEmail();
        $userEmail = $this->get('security.token_storage')->getToken()->getUser()->getEmail();

        $apiUser=$user->getApikey();
        $user2 = $this->get('security.token_storage')->getToken()->getUser()->getApikey();
        if ($apiUser == $user2)
        {
        return $this->view($user);
        }
        else
        {
            throw new Exception("Error, you are ".$userEmail." and you want acces on the page of ".$apiEmail );
        }
    }

    /**
     * EDIT YOUR USER INFORMATION
     * ============================
     * @Rest\Patch("/api/user/{id}/patch")
     * @Rest\View(serializerGroups={"user-info"})
     */
    public function patchApiUser(User $user, Request $request, ValidatorInterface $validator){

        $apiEmail=$user->getEmail();
        $userEmail = $this->get('security.token_storage')->getToken()->getUser()->getEmail();

        $apiUser=$user->getApikey();
        $user2 = $this->get('security.token_storage')->getToken()->getUser()->getApikey();


        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $adress = $request->get('adress');
        $country = $request->get('country');
        $apiKey = $request->get('apiKey');
        if( null !== $firstname )
        {
            $user->setFirstname($firstname);
        }
        if ( null != $lastname)
        {
            $user->setLastname($lastname);
        }
        if ( null != $adress)
        {
            $user->setAdress($adress);
        }
        if ( null != $country)
        {
            $user->setCountry($country);
        }
        if ( null != $apiKey)
        {
            $user->setapiKey($apiKey);
        }

        $validationErrors = $validator->validate($user);
        $errors = array();
        if ($validationErrors->count() > 0) {

            /** @var ConstraintViolation $constraintViolation */
            foreach ($validationErrors as $constraintViolation ){
                $message = $constraintViolation ->getMessage ();
                $propertyPath = $constraintViolation ->getPropertyPath ();
                $errors[] = ['message' => $message , 'propertyPath' => $propertyPath ];
            }
        }
        if (!empty($errors)) {
            throw new BadRequestHttpException(\json_encode( $errors));
        }

        if ($apiUser == $user2) {
            $this->em->persist($user);
            $this->em->flush();
            return $this->json($user);
        }
        else
        {
            throw new Exception("Error, you are ".$userEmail." and you want acces on the page of ".$apiEmail );
        }
    }

    /**
     * CREATE A CARD
     * ============================
     * @Rest\Post("/api/user/{id}/card")
     * @ParamConverter("card", converter="fos_rest.request_body")
     * @Rest\View(serializerGroups={"card-info"})
     */
    public function postApiCard(Card $card, User $user){
        $this->em->persist($card);
        $this->em->flush();

        return "The card has been added";
    }

    /**
     * GET ONE CARD CURRENT USER // USER ==> USER_ID
     * =============================================
     * @Rest\Get("/api/user/{user}/cards/{name}")
     * @Rest\View(serializerGroups={"card-info"})
     */
    public function getApiCard(Card $card, User $user){

        $idUserCurrent=$user->getId();
        $idUser = $this->getUser()->getId();

        if ($idUserCurrent == $idUser) {
            return $this->view($card);
        }
        else
        {
            throw new Exception('%s object not found.');
        }

    }

    /**
     * GET ALL CARD CURRENT USER
     * ============================
     * @Rest\Get("/api/user/{id}/cards")
     * @Rest\View(serializerGroups={"card-info"})
     */
    public function getApiCards(User $user){
        $apiEmail=$user->getEmail();
        $userEmail = $this->get('security.token_storage')->getToken()->getUser()->getEmail();

        $apiUser=$user->getApikey();
        $user2 = $this->get('security.token_storage')->getToken()->getUser()->getApikey();
        if ($apiUser == $user2) {
            return $this->view($user);
        }
        else{
            throw new Exception("Error, you are ".$userEmail." and you want acces on the page of ".$apiEmail );
        }
    }

    /**
     * DELETE ONE CARD CURRENT USER // USER ==> USER_ID
     * =============================================
     * @Rest\Delete("/api/user/{user}/cards/{name}")
     * @Rest\View(serializerGroups={"card-info"})
     */
    public function deleteApiCard(Card $card, User $user, EntityManagerInterface $entityManager){

        $idUserCurrent=$user->getId();
        $idUser = $this->getUser()->getId();

        if ($idUserCurrent == $idUser) {
            $entityManager->remove($card);
            $entityManager->flush();
            return "The card has been deleted";
        }
        else
        {
            throw new Exception('%s object not found.');
        }

    }

}
