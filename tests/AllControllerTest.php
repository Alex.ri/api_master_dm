<?php
namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AllControllerTest extends WebTestCase
{
    /**
     * ======================================
     * GET ONE USER ==> WITH ROLES_USER
     * ======================================
     */
    public function testGetOneUser()
    {
        $client = static::createClient();
        $client->request('GET', '/api/user/test@test.com', [], [],
            ['HTTP_ACCEPT' => 'application/json',
                'HTTP_X-AUTH-TOKEN' => 'test-apikey'
            ]);
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($content);
    }

    /**
     * ============================
     * GET ALL USER
     * ============================
     */
    public function testGetUsers()
    {
        $client = static::createClient();
        $client->request('GET', '/api/anonymous', [], [], ['HTTP_ACCEPT' => 'application/json']);
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($content);
        $arrayContent = \json_decode($content, true);
        $this->assertCount(10, $arrayContent);
    }

    /**
     * ============================
     * GET ALL SUBSCRIPTION
     * ============================
     */
    public function testGetSubscriptions()
    {
        $client = static::createClient();
        $client->request('GET', '/api/anonymous_subs', [], [], ['HTTP_ACCEPT' => 'application/json']);
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($content);
        $arrayContent = \json_decode($content, true);
        $this->assertCount(10, $arrayContent);
    }

    /**
     * ================================
     * GET A USER ==> WITH ROLES_USER
     * ================================
     */
    public function testGetUserUser()
    {
        $client = static::createClient();
        $client->request('GET', '/api/user', [], [],
            ['HTTP_ACCEPT' => 'application/json',
                'HTTP_X-AUTH-TOKEN' => 'test-apikey'
            ]);
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($content);
        $arrayContent = \json_decode($content, true);
        $this->assertCount(10, $arrayContent);
    }

    /**
     * ==============================
     * GET ALL CARD ==> WITH ADMIN
     * ==============================
     */
    public function testGetCard()
    {
        $client = static::createClient();
        $client->request('GET', '/api/admin/cards', [], [],
            ['HTTP_ACCEPT' => 'application/json',
                'HTTP_X-AUTH-TOKEN' => 'admin-apikey'
            ]);
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($content);
        $arrayContent = \json_decode($content, true);
        $this->assertCount(10, $arrayContent);
    }

    /**
     * ============================
     * POST A USER
     * ============================
     */
    public function testPostUsers()
    {
        $client = static::createClient();
        $client->request('POST', '/api/anonymous' , [], [],
            [
                'HTTP_ACCEPT' => 'application/json' ,
                'CONTENT_TYPE' => 'application/json' ,
            ],
            '{"firstname": "aatest","lastname": "bbtest","email": "tes2t@johnson.com","birthday": "12/03/1994"}');
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertEquals (200, $response->getStatusCode());
        $this->assertJson ($content);
    }

    /**
     * ==========================
     * POST A CARD ==> WITH ADMIN
     * ==========================
     */
    public function testPostCard ()
    {
        $client = static::createClient();
        $client->request('POST', '/api/admin/create-card' , [], [],
            [
                'HTTP_ACCEPT' => 'application/json' ,
                'CONTENT_TYPE' => 'application/json' ,
                'HTTP_X-AUTH-TOKEN' => 'admin-apikey'
            ],
            '{"name": "test","creditCardType": "test","creditCardNumber": "123456889102","currencyCode": "RGB", "value":209}');
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertEquals (200, $response->getStatusCode());
        $this->assertJson ($content);
    }

    /**
     * ==================================
     * POST A SUBSCRIPTION ==> WITH ADMIN
     * ==================================
     */
    public function testPostSubscription()
    {
        $client = static::createClient();
        $client->request('POST', '/api/admin/create-sub' , [], [],
            [
                'HTTP_ACCEPT' => 'application/json' ,
                'CONTENT_TYPE' => 'application/json' ,
                'HTTP_X-AUTH-TOKEN' => 'admin-apikey'
            ],
            '{"name": "test","slogan": "test"}');
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertEquals (200, $response->getStatusCode());
        $this->assertJson ($content);
    }

    /**
     * =====================================
     * DELETE ONE USER ==> WITH ROLES_ADMIN
     * =====================================
     */
    public function testDeleteUser()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/admin/8/delete', [], [],
            ['HTTP_ACCEPT' => 'application/json',
                'HTTP_X-AUTH-TOKEN' => 'admin-apikey'
            ]);
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertEquals (200, $response->getStatusCode());
        $this->assertJson ($content);
    }

    /**
     * ====================================
     * DELETE ONE USER ==> WITH ROLES_ADMIN
     * ====================================
     */
    public function testDeleteSubscription()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/admin/4/delete', [], [],
            ['HTTP_ACCEPT' => 'application/json',
                'HTTP_X-AUTH-TOKEN' => 'admin-apikey'
            ]);
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertEquals (200, $response->getStatusCode());
        $this->assertJson ($content);
    }


}