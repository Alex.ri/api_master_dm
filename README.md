## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them?

- PHP 7

----------------------------------------------------------------------

### First of all execute this command

- Execute wamp/ Wampserver/ uWamp or another...

- php bin/console server:run

For run the server in local

- php bin/console d:s:u --force

For update the database schema

- php bin/console hautelook:fixtures:load --purge-with-truncate

For Initialize all datas with fixtures

----------------------------------------------------------------------

### Before execute the PHP UNIT TEST 

Initialize all datas with fixtures to avoid errors

- php bin/console hautelook:fixtures:load --purge-with-truncate

The fixtures will create:  a user test(with fixed datas for php unit test), a admin user & 8 random user with random datas


